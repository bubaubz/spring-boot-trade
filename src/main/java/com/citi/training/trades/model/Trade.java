package com.citi.training.trades.model;

public class Trade {
	
	String stock;
	int id, volume;
	double price;
	
	public Trade() {}
	
	public Trade(int id, String stock, double price, int volume) {
		this.id = id;
		this.stock = stock;
		this.price = price;
		this.volume = volume;
	}
	
	public String getStock() {
		return stock;
	}
	public void setStock(String stock) {
		this.stock = stock;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getVolume() {
		return volume;
	}
	public void setVolume(int volume) {
		this.volume = volume;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}

	@Override
	public String toString() {
		return "Trade:/n stock = "+stock + ", id = "+id+", volume = "+volume+", price = "+price;
	}
	

}


