package com.citi.training.trades.rest;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.citi.training.trades.model.Trade;
import com.citi.training.trades.service.TradeService;

/**
 * The REST Interface Class for {@link com.citi.training.trades.model.Trade} domain object.
 * 
 * @author Administrator
 * 
 * @see Trade
 *  */
@RestController
@RequestMapping("/trade")
public class TradeController {

	private static final Logger LOG = LoggerFactory.getLogger(TradeController.class);
	
	@Autowired
    private TradeService tradeService;
	
	/**
	 * Will return a list of all {@link com.citi.training.trades.model.Trade} in the system
	 * @return each {@link com.citi.training.trades.model.Trade} in the records
	 */
	@RequestMapping(method=RequestMethod.GET,
            produces=MediaType.APPLICATION_JSON_VALUE)
		public List<Trade> findAll(){
			LOG.debug("find all was called");
			return tradeService.findAll();
		}
	
	/**
     * Given an integer id will return the associated {@link com.citi.training.trades.model.Trade} 
     * @param id The id of the {@link com.citi.training.trades.model.Trade} to find
     * @return {@link com.citi.training.trades.model.Trade} found
     */
	@RequestMapping(value="/{id}", method=RequestMethod.GET,
            produces=MediaType.APPLICATION_JSON_VALUE)
		public Trade findById(@PathVariable int id) {
			LOG.debug("create was called, trade: "+id);
			return tradeService.findById(id);
		}
	
	/**
     * For adding a {@link com.citi.training.trades.model.Trade} to the database
     * @param employee {@link com.citi.training.trades.model.Trade}
     * @return {@link com.citi.training.trades.model.Trade} data as a HttpEntity
     */
	@RequestMapping(method=RequestMethod.POST,
            consumes=MediaType.APPLICATION_JSON_VALUE,
            produces=MediaType.APPLICATION_JSON_VALUE)
		public HttpEntity<Trade> create(@RequestBody Trade trade) {
		 	return new ResponseEntity<Trade>(tradeService.create(trade),
		                                     HttpStatus.CREATED);
		}
	
	/**
     * Given an integer id will delete the {@link com.citi.training.trades.model.Trade} associated with that id from the system
     * @param id the id of {@link com.citi.training.trades.model.Trade} to delete
     */
	@RequestMapping(value="/{id}", method=RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public @ResponseBody void deleteById(@PathVariable int id) {
    	LOG.debug("delete was called, trade: "+id);
    	tradeService.deleteById(id);
    }
	
}
