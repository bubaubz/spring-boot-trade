package com.citi.training.trades.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.citi.training.trades.model.Trade;

@Component
public class TradeService {
	
	@Autowired
	private TradeService tradeDao;
	
	 public List<Trade> findAll(){
		 return tradeDao.findAll();
	 }

	 public Trade findById(int id) {
		 return tradeDao.findById(id);
	 }

	 public Trade create(Trade trade) {
		 if(trade.getStock().length() > 0) {
			 return tradeDao.create(trade);
		 }
		 throw new RuntimeException("invalid Parameter: trade name: "+trade.getStock());
	 }

	 public void deleteById(int id) {
		 tradeDao.deleteById(id);
	 }

}
