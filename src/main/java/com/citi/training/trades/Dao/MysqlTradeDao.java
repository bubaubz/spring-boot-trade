package com.citi.training.trades.Dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Component;

import com.citi.training.trades.exceptions.TradeNotFoundException;
import com.citi.training.trades.model.Trade;
import com.citi.training.trades.rest.TradeController;


@Component
public class MysqlTradeDao implements TradeDao {
	
	@Autowired
	JdbcTemplate tpl;
	
	/**
	 * Returns all trades in the system as a List
	 */
	public List<Trade> findAll(){
		return tpl.query("SELECT id, stock, price, volume FROM trade", new TradeMapper());
	}
	
	/**
	 * Given an int, id, will return the {@link com.citi.training.trades.model.Trade} associated with that
	 */
    public Trade findById(int id) {
    	List<Trade> employees = tpl.query("SELECT id, stock, price, volume FROM trade WHERE id = ?",
    			new Object[] {id},
    			new TradeMapper()
    			);
    	if(employees.size()<= 0) {
    		throw new TradeNotFoundException("Trade id: "+id+" doesn't exist!");
    	}
    	
    	return employees.get(0);
    	
    	}
    
    /**
     * This method when given an {@link com.citi.training.trades.model.Trade} and add to database
     * @return {@link com.citi.training.trades.model.Trade} created
     */
    @Override
    public Trade create(Trade trade) {
        KeyHolder keyHolder = new GeneratedKeyHolder();
        this.tpl.update(
            new PreparedStatementCreator() {
                @Override
                public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
                    PreparedStatement ps =
                            connection.prepareStatement("insert into trade (stock, price, volume) values (?, ?, ?)",
                            Statement.RETURN_GENERATED_KEYS);
                    ps.setString(1, trade.getStock());
                    ps.setDouble(2, trade.getPrice());
                    ps.setInt(3, trade.getVolume());
                    return ps;
                }
            },
            keyHolder);
        trade.setId(keyHolder.getKey().intValue());
        return trade;
    }
    
    /**
     * When given an int, id, this method deleted the {@link com.citi.training.trades.model.Trade} associated with that id from the database
     */
    public void deleteById(int id) {
    	
    	findById(id);
    	
    	tpl.update("DELETE FROM trade WHERE id =?", id);
    
    }
	
	/**
	 * Mapper class for converting SQL to trade objects
	 * @author Administrator
	 *
	 */
	public static final class TradeMapper implements RowMapper<Trade>{
		
		private static final Logger LOG = LoggerFactory.getLogger(TradeController.class);
		
		/**
		 * Given the result set to an SQL statement will return the data as {@link com.citi.training.trades.model.Trade} objects
		 */
		public Trade mapRow(ResultSet rs, int rowNum) throws SQLException{
		
			LOG.debug("Converting trade from db row: "+rowNum);
			return new Trade(rs.getInt("id"), rs.getString("stock"), rs.getDouble("price"), rs.getInt("volume"));
		}
	}
}
