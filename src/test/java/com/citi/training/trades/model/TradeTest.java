package com.citi.training.trades.model;

import static org.junit.Assert.*;

import org.junit.Test;

public class TradeTest {
	
	private String testStock = "GOOGL";
	private int testId = 1, testVolume = 100;
	private double testPrice = 4.99;

	@Test
	public void testTradeCondstructor() {
		Trade testTrade = new Trade(testId,testStock,testPrice,testVolume);
		
		assertEquals(testId, testTrade.getId());
		assertEquals(testStock, testTrade.getStock());
		assertEquals(testVolume, testTrade.getVolume());
		assertEquals(testPrice, testTrade.getPrice(), 0.001);
	}

	@Test
	public void testGetSetStock() {
		Trade testTrade = new Trade();
		
		testTrade.setStock(testStock);
		
		assertEquals(testStock, testTrade.getStock());
	}

	@Test
	public void testGetSetId() {
		Trade testTrade = new Trade();
		
		testTrade.setId(testId);
		
		assertEquals(testId, testTrade.getId());
	}

	@Test
	public void testGetSetVolume() {
		Trade testTrade = new Trade();
		
		testTrade.setVolume(testVolume);
		
		assertEquals(testVolume, testTrade.getVolume());
	}

	@Test
	public void testGetSetPrice() {
		Trade testTrade = new Trade();
		
		testTrade.setPrice(testPrice);
		
		assertEquals(testPrice, testTrade.getPrice(), 0.001);
	}

}
